;Geferson Luis Hess Jùnior  - 205956
;Gerador de números aleatórios de 10 bits.
assume cs:codigo,ds:dados,es:dados,ss:pilha

CR       EQU    0DH ; constante - codigo ASCII do caractere "carriage return"
LF       EQU    0AH ; constante - codigo ASCII do caractere "line feed"
ESCAPE   EQU    1BH ; constante - codigo ASCII do caractere "escape"
ENTER0	 EQU	0DH ; constante - codigo ASCII do caractere "enter"
BACKSPACE EQU   8H  ; constante - código ASCII do caractere "backspace"

; definicao do segmento de dados do programa
dados    segment
nome_arq  db	13 dup(0)
semente   db	5 dup(0)
	  db	'$'
sequencia db	5 dup(0)
	  db	'$'
contador  dw	0
caractere db	?	;Caractere que será escrito na tela
	  db	'$'
handler	  dw	?
linha1    db    '*------------------------------------------------------------------------------*'
linha2    db    '|        GERADOR DE NUMEROS PSEUDOALEATORIOS DE 10 BITS (Geferson Hess Junior) |'
linha3	  db	'*----------------------------------*----------------------*--------------------*'
linha4	  db	'| Arquivo de entrada:              | Sequencia de      valores - semente:      |'
linha5	  db	'*----------------------------------*----------------------*--------------------*'
linha6	  db	'|                  1         2         3         4         5         6         |'
linha7	  db	'|        0123456789012345678901234567890123456789012345678901234567890123      |'
linha8	  db	'|      0 ................................................................      |'
linha9	  db	'|     64 ................................................................      |'
linha10	  db	'|    128 ................................................................      |'
linha11	  db	'|    192 ................................................................      |'
linha12	  db	'|    256 ................................................................      |'
linha13	  db	'|    320 ................................................................      |'
linha14	  db	'|    384 ................................................................      |'
linha15	  db	'|    448 ................................................................      |'
linha16	  db	'|    512 ................................................................      |'
linha17	  db	'|    576 ................................................................      |'
linha18	  db	'|    640 ................................................................      |'
linha19	  db	'|    704 ................................................................      |'
linha20	  db	'|    768 ................................................................      |'
linha21	  db	'|    832 ................................................................      |'
linha22	  db	'|    896 ................................................................      |'
linha23	  db	'|    960 ................................................................      |'
linha24   db	'|                                                                              |'
linha25   db	'*------------------------------------------------------------------------------$'
erro	  db	'Erro na abertura do arquivo de entrada. Tecle ENTER para digitar outro nome.$'
linha4x	  db	'| Arquivo de entrada:              | Sequencia de      valores - semente:      |'
	  db	'$'
buffer	  db	64 dup(?)
msgfimu	  db	'|                    Programa finalizado pelo usuario.                         |'
	  db	'$'
msgfima	  db	'|                Programa finalizado pois acabou o arquivo.                    |'
	  db	'$'
vsemente  dw	0
vsequencia dw	0
vlinha0	   db	64 dup('.'),'$'
vlinha64   db	64 dup('.'),'$'
vlinha128  db	64 dup('.'),'$'
vlinha192  db	64 dup('.'),'$'
vlinha256  db	64 dup('.'),'$'
vlinha320  db	64 dup('.'),'$'
vlinha384  db	64 dup('.'),'$'
vlinha448  db	64 dup('.'),'$'
vlinha512  db	64 dup('.'),'$'
vlinha576  db	64 dup('.'),'$'
vlinha640  db	64 dup('.'),'$'
vlinha704  db	64 dup('.'),'$'
vlinha768  db	64 dup('.'),'$'
vlinha832  db	64 dup('.'),'$'
vlinha896  db	64 dup('.'),'$'
vlinha960  db	64 dup('.'),'$'
auxprandom dw	0
auxsemente dw	0
auxsemente1 dw	0
cont_seq   db	0
cont_sem   db	0
aux  	   dw	0
aux_fim	   db	0
zeroinsig  db	0
leu_carc   db	0

dados    ends

; definicao do segmento de pilha do programa
pilha    segment stack ; permite inicializacao automatica de SS:SP
         dw     128 dup(?)
pilha    ends
         
; definicao do segmento de codigo do programa
codigo   segment
inicio:  ; CS e IP sao inicializados com este endereco
         mov    ax,dados  ; inicializa DS
         mov    ds,ax     ; com endereco do segmento DADOS
         mov    es,ax     ; idem em ES
; fim da carga inicial dos registradores de segmento

; a partir daqui, as instrucoes especificas para cada programa	
comeco:
	mov	contador,0		;Zera o contador.
	lea	di,nome_arq		
	mov byte ptr [di],0	
	inc	di
	mov byte ptr [di],0	
	inc	di
	mov byte ptr [di],0	
	inc	di
	mov byte ptr [di],0	
	inc	di
	mov byte ptr [di],0	
	inc	di
	mov byte ptr [di],0		;Apaga o nome do arquivo.	
	inc	di
	mov byte ptr [di],0	
	inc	di
	mov byte ptr [di],0	
	inc	di
	mov byte ptr [di],0			
	inc	di
	mov byte ptr [di],0		
	inc	di
	mov byte ptr [di],0		
	inc	di
	mov byte ptr [di],0		
	inc	di
	mov byte ptr [di],0
	mov	ch,0
	mov	cl,0
	mov	dh,24
	mov 	dl,79			;Define janela com fundo cinza claro e letras azul
	mov	al,0
	mov	bh,71H			
	mov	ah,6			
	int	10h			
	
	mov	bh,0			
	mov	dl,0			
	mov	dh,0			;Posiciona o cursor no ínicio da janela.
	mov	ah,2			
	int	10h

	lea	dx,linha1		;Aponta o ínicio do tabuleiro.
	call 	escreve			;Escreve o tabuleiro na tela.
aqui:	mov	dl,22			;Posiciona o cursor na coluna 22
	mov	dh,3			;e na linha 3.
	mov	ah,2			
	int	10h			;Posiciona o cursor.
	lea	di,nome_arq		;Aponta DI para o ínicio da string que irá armazenar o nome do arquivo.
	
	mov 	contador,0
le_nome:
	mov	ah,0			;Lê a tecla, mas não escreve na tela.
	int	16h			
	cmp	al,ENTER0		;Se for enter, testa se é um arquivo válido ou inválido
	je	tstnome
	
	cmp	al,BACKSPACE 		;Se for backspace, testa se pode apagar.
	je	tstapagar
	
	cmp 	contador,12		;Se o contador tiver o valor 8, o arquivo atingiu o seu valor máximo.
	je	le_nome
		
	mov	caractere,al		;Move o código ASCII do caractere para ser escrito na tela.
	lea	dx,caractere		;Aponta dx para o caractere.
	call	escreve
	
	inc	contador		;Incrementa o contador se foi escrito um novo valor.
			
	mov	[di],al			;Salva o valor na string que contém o nome do arquivo.
	inc	di			;Incrementa DI, fazendo-o apontar para a próxima "posição" na string
	
	jmp	le_nome
	
tstapagar:
	cmp	contador,0		;Se o contador tiver o valor 0, significa que o cursor está na coluna 22	
	je	le_nome			;Então não pode apagar
	
	mov	caractere,08
	lea	dx,caractere		;Ecoa na tela um backspace, recuando o cursor.
	call	escreve
	mov	caractere,20h		;Insere um espaço em branco na tela.
	lea	dx,caractere
	call 	escreve
	mov	caractere,08		;Volta a recuar pois o ultimo escreve avançou o cursor.
	lea	dx,caractere
	call	escreve			
	dec	contador		;Diminui o valor do contador.
	dec	di			
	mov byte ptr [di],20H		;Apaga o valor também na string que contém o nome do arquivo.
	jmp	le_nome			;Volta para ler novas teclas.
	
tstnome:
	lea	di,nome_arq		;Aponta DI para o a string que irá conter o nome do arquivo.
	add	di,contador			
	sub	di,4			;Recua três posições.
	cmp byte ptr[di],2EH	    	;Compara se o valor é . (ponto final). Se sim, abre o arquivo. Se não, insere ".txt" no fim.
	je	abrearq
	add	di,4			;Avança até a posição final na string e mais uma.
	mov byte ptr [di],2EH		;Insere um "." (ponto final) no valor apontado por DI.
	inc	di			
	mov byte ptr [di],74H		;Insere um "t" no valor apontado por DI.
	inc	di
	mov byte ptr [di],78H		;Insere um "x" no valor apontado por DI.
	inc	di			
	mov byte ptr [di],74H		;Insere um "t" no valor apontado por DI.

abrearq:
	mov	ah,3dh			;Abrir arquivo
	mov	al,0			;Modo: Leitura
	lea	dx,nome_arq		;Aponta DX para a string que contém o nome do arquivo.
	int	21h
	jnc	abriu
	
	mov	dl,2			
	mov	dh,3			;Posiciona o cursor.
	mov	ah,2
	int	10h			
	lea	dx,erro			;Aponta DX para a mensagem de erro.
	call	escreve			;Escreve a mensagem de erro na tela.
	
espera_enter:
	mov	ah,0			;Lê a tecla, mas não escreve na tela.
	int	16h			
	cmp	al,ENTER0		;Se for enter, volta a escrever a linha 3
	jne	espera_enter
	mov	dl,0			
	mov	dh,3			;Posiciona o cursor.
	mov	ah,2
	int	10h	
	
	lea	dx,linha4x
	call 	escreve			;Reescreve a linha 3.
	
	mov	bh,0			
	mov	dl,50			
	mov	dh,3			;Posiciona o cursor no local onde deve ser salva a sequencia.
	mov	ah,2			
	int	10h
	
	lea	dx,sequencia		;Escreve a sequencia.
	call 	escreve
	
	mov	bh,0			
	mov	dl,74			
	mov	dh,3			;Posiciona o cursor no local onde deve ser digitada a semente.
	mov	ah,2			
	int	10h

	lea	dx,semente		;Escreve a semente.
	call 	escreve
	jmp	aqui			
abriu:
	mov	handler,ax		;Salva o handler do arquivo.
	lea	di,sequencia		;Aponta DI para a string que irá armazenar o valor lido

nova_sequencia:
	lea	di,sequencia
	mov byte ptr [di],0
	inc	di
	mov byte ptr [di],0
	inc	di
	mov byte ptr [di],0		;Zera o valor salvo em sequencia.
	inc	di
	mov byte ptr [di],0
	
	mov	cont_seq,0		;Zera os contadores.
	mov	cont_sem,0
	
	lea	di,sequencia
	
	mov	zeroinsig,0		;Zera a flag de zeros insignificantes.
	mov	leu_carc,0
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
	
laco_sequencia:	
	mov 	ah,3fh			
	mov	bx,handler		;Lê um bit do arquivo
	mov	cx,1			
	lea	dx,buffer	
	int	21h
	
	cmp	ax,cx			;Testa se ax leu 1 bit
	jne	aux_fim1		;Se ele não leu 1 bit, então houve erro de execução
	
	cmp	buffer,48
	je	insig0			;Se for zero, testa se é insignificante.
	
	cmp	buffer,20H			
	je	laco_sequencia		;Se o bit lido for um espaço em branco, deve ser ignorado
	
	cmp  	buffer,2CH			
	je	ssss			;Se o bit lido for um uma "," (vírgula). O programa deve começar a ler o valor da semente.

continua1:     
	mov	dl,buffer
	mov 	[di],dl			;Salva o valor na memória
	inc	di			;Aponta DI para o próximo valor.
	inc	cont_seq
	inc	zeroinsig		
	inc	leu_carc		;Leu um caractere.
	jmp	laco_sequencia

aux_fim1:
	jmp	auxfim
		
insig0:
	cmp	zeroinsig,0
        je	laco_sequencia
	jmp	continua1     

ssss:
	lea	di,semente
	
	mov byte ptr [di],0
	inc	di
	mov byte ptr [di],0
	inc	di
	mov byte ptr [di],0			;Zera o valor salvo em sequencia.
	inc	di
	mov byte ptr [di],0
	
        lea	di,semente		;Aponta DI para a string que irá armazenar o valor lido
        mov	zeroinsig,0
        
laco_semente:
	mov	ah,3fh
	mov	bx,handler
	mov	cx,1			;Lê um bit do arquivo
	lea	dx,buffer
	int	21h
	
	cmp	ax,cx			;Testa se leu 1 bit.
	jne	auxfim			;Se não leu 1 bit, então houve erro de execução.

	cmp	buffer,48
	je	insig1			;Se for zero, testa se é insignificante.
	
	cmp 	buffer,CR		
	je	gerar			;Vai para a PRANDOM e gera os valores.
	
	cmp	buffer,20H		;Se for um espaço em branco, ignora.
	je	laco_semente

continua2:
	mov	dl,buffer
	mov 	[di],dl
	inc	di			;Aponta DI para o próximo valor.
	inc	cont_sem
	inc	zeroinsig
	jmp	laco_semente

insig1:
        cmp	zeroinsig,0
        je	laco_semente
        jmp	continua2     
	
auxfim:
	inc	aux_fim
	cmp	leu_carc,0
	je	ehfimaux
	jmp	gerar
	
ehfimaux:
	jmp 	nova
	
fim_arquivo:
	jmp	le_nome
gerar:	

	mov	ah,3fh
	mov	bx,handler
	mov	cx,1			;Lê o bit LF do arquivo, caso contário ele fará parte do valor da semente.
	lea	dx,buffer
	int	21h
  
	;cmp	leu_carc,0
	;je	nova_sequencia_aux
	
	mov	bh,0			
	mov	dl,50			
	mov	dh,3			;Posiciona o cursor no local onde deve ser salva a sequencia.
	mov	ah,2			
	int	10h
	
	lea	dx,sequencia		;Escreve a sequencia.
	call 	escreve
	
	mov	bh,0			
	mov	dl,74			
	mov	dh,3			;Posiciona o cursor no local onde deve ser digitada a semente.
	mov	ah,2			
	int	10h

	lea	dx,semente		;Escreve a semente.
	call 	escreve
	
	mov 	vsemente,0		;Zera a variável.
	mov 	vsequencia,0		
      
      	lea	di,sequencia	
	cmp	cont_seq,1
	je	eh1
	cmp	cont_seq,2
	je	eh2
	cmp	cont_seq,3
	je	eh3
	
	mov 	al,byte ptr [di]
	sub	al,48			;Subtrai 48, transformando para o valor em binário.
	mov 	ah,0
	mov 	dx,1000
	mul	dx			;Multiplica por 1000.
	add     vsequencia,ax		;Soma a dezena no valor da variável.
	inc	di			;Faz DI apontar para a centena.
eh3:	
	mov 	al,byte ptr [di]
	sub	al,48			;Subtrai 48, transformando para o valor em binário.
	mov 	ah,0
	mov 	dx,100
	mul	dx			;Multiplica por 100.
	add     vsequencia,ax		;Soma a dezena no valor da variável.
	inc	di			;Faz DI apontar para a dezena
eh2:	
	mov 	al,byte ptr [di]
	sub	al,48			;Subtrai 48, transformando para o valor em binário.
	mov 	ah,0
	mov 	dx,10
	mul	dx			;Multiplica por 10.
	add     vsequencia,ax		;Soma a dezena no valor da variável
 	inc	di			;Faz DI apontar para a dezena
	
eh1:
	mov 	al,byte ptr [di] 
	sub     al,48			;Subtrai 48, transformando para o valor em binário.
	mov     ah,0
	add	vsequencia,ax		;Soma o valor na variável.
    
insere_semente:
	lea	di,semente	
	cmp	cont_sem,1
	je	eh11
	cmp	cont_sem,2
	je	eh12
	cmp	cont_sem,3
	je	eh13

	mov 	al,byte ptr [di]
	sub	al,48			;Subtrai 48, transformando para o valor em binário.
	mov 	ah,0
	mov 	dx,1000
	mul	dx			;Multiplica por 1000.
	add     vsemente,ax		;Soma a dezena no valor da variável.
	inc	di			;Faz DI apontar para a centena.
eh13:	
	mov 	al,byte ptr [di]
	sub	al,48			;Subtrai 48, transformando para o valor em binário.
	mov 	ah,0
	mov 	dx,100
	mul	dx			;Multiplica por 100.
	add     vsemente,ax		;Soma a dezena no valor da variável.
	inc	di			;Faz DI apontar para a dezena
eh12:	
	mov 	al,byte ptr [di]
	sub	al,48			;Subtrai 48, transformando para o valor em binário.
	mov 	ah,0
	mov 	dx,10
	mul	dx			;Multiplica por 10.
	add     vsemente,ax		;Soma a dezena no valor da variável
	inc	di			;Faz DI apontar para a dezena
eh11:
	mov 	al,byte ptr [di] 
	sub     al,48			;Subtrai 48, transformando para o valor em binário.
	mov     ah,0
	add	vsemente,ax		;Soma o valor na variável.

apos_random:
	mov	auxprandom,0		;Zera o contador auxiliar
	mov	aux,0
	cmp	vsequencia,0
	je	le_novo_valor
coloca_valor:	
	mov	dx,vsemente
	cmp	dx,auxprandom		;Compara a semente com o valor de cada linha, até encontrar o valor em que ele será menor.
	je	ehigual			;Se for igual, irá salvar o valor no primeiro elemento da linha.
	
	cmp	dx,auxprandom
	jl	achou_linha		;Se for menor, então achou a linha em que deve estar o valor.
	
	add	auxprandom,64		;Se for maior, então soma 64.
	inc	aux			;Conta quantos $ já foram computados, para somar depois
	jmp	coloca_valor		;Refaz os testes.

le_novo_valor:
	jmp	imprimir_linhas		;Volta a ler uma nova linha no arquivo.

ehigual:
	lea	di,vlinha0
	add 	di,auxprandom		;Desloca DI para posição inical e salva o valor.			
	add     di,aux			;Soma o descolamento devido aos $.
	
	cmp byte ptr [di],57		;Se o valor for 9, então insere 0 no valor.
	je	ehzero
	
	cmp byte ptr [di],46		;Se for um ponto, insere 1.
	je	ehponto
	
	cmp byte ptr [di],48		;Se for 0, insere 1.
	je	ehponto
	
	add byte ptr [di],1		;Soma um no valor.
	jmp	prandom

ehponto:
	mov byte ptr [di],49		;Insere 1 no valor.
	jmp	prandom			
	
ehzero:
	mov byte ptr [di],48		;Zera o valor
	jmp	prandom
	
achou_linha:
	lea	di,vlinha0
	sub	auxprandom,64		;Recua uma linha, pois é na anterior que o valor deve ser escrito.
	add	di,auxprandom		;Posiciona DI no inicio da linha.
	add	di,aux 			;Soma o descolamento devido as $.
	dec	di
	
	mov	ax,vsemente
	mov	dx,auxprandom		
	
	sub	ax,dx			;Subtrai o valor da semente do valor inicial da linha.
	
	add	di,ax			;Soma essa diferença no valor de DI, com isso DI irá apontar para o valor correto.
		
	cmp byte ptr[di],57		;Se o valor for 9, então insere 0.
	je	ehzero			
	
	cmp byte ptr [di],46		;Se for um ponto, insere 1.
	je	ehponto
	
	cmp byte ptr [di],48		;Se for 0, insere 1.
	je	ehponto
	
	add byte ptr [di],1		;Soma o valor já em ASCII.
	jmp	prandom

prandom:
	mov	auxsemente,0		;Zera a variável auxilar.
	mov	auxsemente1,0
	mov	dx,vsemente		
	and	dx,1			;Isola o bit 0.
	cmp	dx,0			
	je	pzero			
	inc	auxsemente		;Auxsemente recebe o valor 1.
pzero:
	mov	dx,vsemente
	and	dx,128			;Isola o bit 7 e salva em dx.
	cmp	dx,0
	je	pzero1
	inc	auxsemente1		;Auxsemente1 recebe o valor 1.
pzero1:	
	mov	ax,auxsemente
	xor	auxsemente1,ax		
	cmp	auxsemente1,0
	je	pzero2
	mov	dx,vsemente
	shr	dx,1
	or	dx,512			;Insere o restultado no bit 9.
	mov	vsemente,dx
	dec	vsequencia
	jmp	apos_random
pzero2:	
	mov	dx,vsemente
	shr	dx,1			;Roda
	mov	vsemente,dx		;Salva o valor na semente.
	dec	vsequencia		;Reduz o claco_sequenciaontador do número de sequencias.
	jmp	apos_random		
	
imprimir_linhas:		
	
	mov	bh,0				
	mov	dl,9			
	mov	dh,7			;IMPRIMI AS LINHAS
	mov	ah,2			
	int	10h
	lea	dx,vlinha0		
	call	escreve
	
	mov	bh,0				
	mov	dl,9			
	mov	dh,8			
	mov	ah,2			
	int	10h
	lea	dx,vlinha64		
	call	escreve
	
	mov	bh,0				
	mov	dl,9			
	mov	dh,9			
	mov	ah,2			
	int	10h
	lea	dx,vlinha128		
	call	escreve
	
	mov	bh,0				
	mov	dl,9			
	mov	dh,10			
	mov	ah,2			
	int	10h
	lea	dx,vlinha192		
	call	escreve
	
	mov	bh,0				
	mov	dl,9			
	mov	dh,11			
	mov	ah,2			
	int	10h
	lea	dx,vlinha256		
	call	escreve
	
	mov	bh,0				
	mov	dl,9			
	mov	dh,12			
	mov	ah,2			
	int	10h
	lea	dx,vlinha320		
	call	escreve
	
	mov	bh,0				
	mov	dl,9			
	mov	dh,13			
	mov	ah,2			
	int	10h
	lea	dx,vlinha384		
	call	escreve
	
	mov	bh,0				
	mov	dl,9			
	mov	dh,14			
	mov	ah,2			
	int	10h
	lea	dx,vlinha448		
	call	escreve
	
	mov	bh,0				
	mov	dl,9			
	mov	dh,15			
	mov	ah,2			
	int	10h
	lea	dx,vlinha512		
	call	escreve
	
	mov	bh,0				
	mov	dl,9			
	mov	dh,16			
	mov	ah,2			
	int	10h
	lea	dx,vlinha576		
	call	escreve
	
	mov	bh,0				
	mov	dl,9			
	mov	dh,17			
	mov	ah,2			
	int	10h
	lea	dx,vlinha640		
	call	escreve
	
	mov	bh,0				
	mov	dl,9			
	mov	dh,18			
	mov	ah,2			
	int	10h
	lea	dx,vlinha704		
	call	escreve
	
	mov	bh,0				
	mov	dl,9			
	mov	dh,19			
	mov	ah,2			
	int	10h
	lea	dx,vlinha768		
	call	escreve
	
	mov	bh,0				
	mov	dl,9			
	mov	dh,20			
	mov	ah,2			
	int	10h
	lea	dx,vlinha832		
	call	escreve
	
	mov	bh,0				
	mov	dl,9			
	mov	dh,21			
	mov	ah,2			
	int	10h
	lea	dx,vlinha896		
	call	escreve
	
	mov	bh,0				
	mov	dl,9			
	mov	dh,22			
	mov	ah,2			
	int	10h
	lea	dx,vlinha960		
	call	escreve

	espera_tecla1:
	mov	ah,0			;Lê a tecla, mas não escreve na tela.
	int	16h			
	cmp	al,ENTER0		;Se for enter gera as novas sequencias.
	je	nova
	
	cmp	al,ESCAPE
	je	ehfim			;Se for ESC, o usuário encerrou.
	
	jmp	espera_tecla1
	
nova:	cmp	aux_fim,1
	je	ehfim1			;Testa se o arquivo já acabou.
	jmp 	nova_sequencia

ehfim1:
	mov	dl,0			
	mov	dh,3			;Posiciona o cursor.
	mov	ah,2
	int	10h	
	
	lea	dx,msgfima
	call 	escreve
	
	mov	dl,79			
	mov	dh,24			;Posiciona o cursor.
	mov	ah,2
	int	10h
	
	jmp	fim

ehfim:
	mov	dl,0			
	mov	dh,3			;Posiciona o cursor.
	mov	ah,2
	int	10h	
	
	lea	dx,msgfimu
	call 	escreve
	
	mov	dl,79			
	mov	dh,24			;Posiciona o cursor.
	mov	ah,2
	int	10h
	
	jmp	fim

	
espera proc
	mov	ah,1     		 ; le um caractere do teclado e escreve na tela
        int	21h      		 ; caractere lido volta no registrador AL
        cmp    al,ESCAPE 		; se ESC, termina a execucao do programa
        je     fim
        jmp    espera 
	ret
espera endp

;*******Escreve na tela**********
escreve	proc	;Assume que dx já está apontando para a mensagem.
	mov 	ah,9		;Função que escreve na tela
	int 	21h		;Chamada de vídeo
	ret
escreve endp

; retorno ao DOS com codigo de retorno 0 no AL (fim normal)
fim:
	 mov	ah,3eh		   ;Fecha o arquivo
	 mov	bx,handler
	 int	21h
	 
         mov    ax,4c00h           ; funcao retornar ao DOS no AH
         int    21h                ; chamada do DOS

codigo   ends

; a diretiva a seguir indica o fim do codigo fonte (ultima linha do arquivo)
; e informa que o programa deve comear a execucao no rotulo "inicio"
         end    inicio 

 
